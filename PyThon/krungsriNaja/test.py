import time
import datetime

# s = "02-025-834381-1"
# s = "".join(s.split("-"))
# print(s)
def in_between(now, start, end):
    if start <= end:
        return start <= now < end
    else:
        return start <= now or now < end


s = 1608792507.494511
s1 = 1607792507.494511
s2 = 1607892507.494511
print(s)
tmp = in_between(s, s1, s2)
print(tmp)


# date_time_str = "2018-06-29 08:15:27.243860"
# date_time_obj = datetime.datetime.strptime(date_time_str, "%Y-%m-%d %H:%M:%S.%f")
# print(date_time_obj)