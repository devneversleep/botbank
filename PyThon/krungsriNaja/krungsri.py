import Drive_Control
import time
import datetime
import xlsxwriter
import random
import pandas as pd
from random import randint
from datetime import date
import os

Acc = "7181189435"
BankType = "Krungsri"


def scrollUpAndDown():
    for n in range(random.randint(8, 12)):
        option = random.randint(0, 1)
        if option:
            dc.keypressDOWN()
        else:
            dc.keypressUP()
        time.sleep(random.randint(1, 10) / 10)


def goHome():
    while 1:
        print("go home baby!")
        time.sleep(random.randint(1, 3))
        gohome = dc.find_xpath("//*[@id='ctl00_pnlMainMenu']/div/a[1]")
        # gohome = dc.find_xpath("/html/body/div/form/div[6]/div/div[1]/div[4]/div/a[1]")
        if len(gohome) > 0:
            gohome[0].click()
            break


def zero():
    print("Youe case zero")
    dc.click_xpath("//*[@class='inbox_button']")
    time.sleep(random.randint(2, 3))
    dc.click_xpath("//*[@class='pagination_next']")
    time.sleep(random.randint(2, 3))
    dc.click_xpath("//*[@class='pagination_previous']")
    time.sleep(random.randint(2, 3))
    while 1:
        element = dc.find_xpath("//*[@id='ctl00_pnlMainMenu']/div/div[3]")
        if len(element) > 0:
            element[0].click()
            break


def one():
    print("You case one")
    time.sleep(random.randint(1, 3))
    dc.click_xpath("/html/body/div/form/div[6]/div/div[1]/div[4]/div/a[2]")
    time.sleep(random.randint(1, 3))
    scrollUpAndDown()
    goHome()
    while 1:
        element = dc.find_xpath("//*[@id='ctl00_pnlMainMenu']/div/div[3]")
        if len(element) > 0:
            element[0].click()
            break
    # dc.click_xpath("/html/body/div/form/div[5]/div/a")
    time.sleep(random.randint(1, 3))


def two():
    print("You case two")
    time.sleep(1)
    element = dc.click_xpath(
        "/html/body/div/form/div[6]/div/div[2]/div[2]/div[2]/div/div"
    )
    time.sleep(random.randint(1, 3))
    dc.click_xpath("/html/body/div/form/div[6]/div/div[1]/div[3]/div[3]/input[1]")
    time.sleep(random.randint(1, 3))
    for n in range(random.randint(8, 12)):
        dc.keypressDOWN()
        time.sleep(random.randint(1, 10) / 10)
    scrollUpAndDown()
    goHome()
    while 1:
        element = dc.find_xpath("//*[@id='ctl00_pnlMainMenu']/div/div[3]")
        if len(element) > 0:
            element[0].click()
            break


def three():
    print("You case three")
    dc.click_xpath("/html/body/div[1]/form/div[6]/div/div[1]/div[4]/div/a[1]")
    time.sleep(1)
    dc.click_xpath("//a[@id='ctl00_lbtnMoreContact']")
    time.sleep(random.randint(3, 5))
    while 1:
        element = dc.find_xpath("//*[@id='ctl00_pnlMainMenu']/div/div[3]")
        if len(element) > 0:
            element[0].click()
            break
    scrollUpAndDown()


def four():
    # goHome()
    print("You case four")
    time.sleep(random.randint(2, 3))
    dc.click_xpath("/html/body/div/form/div[6]/div/div[1]/div[4]/div/div[1]")
    time.sleep(random.randint(1, 3))
    dc.click_xpath("/html/body/div/form/div[6]/div/div[1]/div[4]/div/div[2]/a")
    for n in range(random.randint(8, 12)):
        dc.keypressDOWN()
        time.sleep(random.randint(1, 10) / 10)
    time.sleep(random.randint(1, 3))
    for n in range(random.randint(8, 12)):
        dc.keypressUP()
        time.sleep(random.randint(1, 10) / 10)
    goHome()


def save_statement(dataAccMe, dataBankType, dataDate, dataTime, dataMoney, dataAcc):
    today = date.today()

    # สร้าง DataFrame ที่มี 1 คอลัมน์ชื่อ 'Data'
    dataframe3 = pd.DataFrame(
        {
            "Acc_Me": dataAccMe,
            "BankType": dataBankType,
            "Date": dataDate,
            "Time": dataTime,
            "Money": dataMoney,
            "Acc": dataAcc,
        }
    )

    # สร้าง Pandas Excel Writer เพื่อใช้เขียนไฟล์ Excel โดยใช้ Engine เป็น xlsxwriter
    # โดยตั้งชื่อไฟล์ว่า 'simple_data.xlsx'
    writer = pd.ExcelWriter("krungsri_statement_" + str(date.today()) +".xlsx", engine="xlsxwriter")

    # นำข้อมูลที่สร้างไว้ในตัวแปร dataframe เขียนลงไฟล์
    dataframe3.to_excel(writer, sheet_name="kbank")

    # จบการทำงาน Pandas Excel writer และเซฟข้อมูลออกมาเป็นไฟล์ Excel
    writer.save()
    print("Done.")


def send_keys_element(element, command):
    for x in command:
        element.send_keys(str(x))
        time.sleep(random.randint(0, 10) / 10)


def login(website, username, password):
    # while 1:
    time.sleep(random.randint(1, 2))
    dc.open_web(website)
    time.sleep(random.randint(1, 3))
    username_element = dc.find_name("ctl00$cphLoginBox$txtUsernameSME")
    send_keys_element(username_element, username)
    time.sleep(random.randint(1, 3))
    password_element = dc.find_name("ctl00$cphLoginBox$txtPasswordSME")
    send_keys_element(password_element, password)
    time.sleep(random.randint(1, 3))
    dc.click_xpath("//input[@name='ctl00$cphLoginBox$imgLogin']")
    time.sleep(random.randint(1, 10))
    return True


def randomshit(options):
    randomNum = random.randint(0, len(options) - 1)
    options[randomNum]()

def Transfer_money():
    try:
        dc.click_xpath('//*[@id="ctl00_lbtnLogoHomePage"]/div/img')
        time.sleep(3)
        dc.click_xpath(' //*[@id="ctl00_pnlMainMenu"]/div/div[4]/div[2]/a[1]/div')
        time.sleep(3)
        dc.click_xpath('//*[@id="ctl00_cphSectionData_rptAccTo_ctl01_imgCard"]')
        time.sleep(3)
        money_input = dc.find_name("ctl00$cphSectionData$txtAmountTransfer")
        time.sleep(3)
        send_keys_element(money_input, str(random.randint(2, 3) / 1.7))
        time.sleep(3)
        dc.click_xpath('//*[@id="ctl00_cphSectionData_btnSubmit"]')
        time.sleep(3)
        otp = input("input OTP = ")
        time.sleep(3)
        input_OTP = dc.find_name("ctl00$cphSectionData$OTPBox1$txtOTPPassword")
        time.sleep(3)
        send_keys_element(input_OTP, otp)
        time.sleep(3)
        dc.click_xpath('//*[@id="ctl00_cphSectionData_OTPBox1_btnConfirm"]')
        time.sleep(3)
        dc.click_xpath('//*[@id="ctl00_lbtnLogoHomePage"]/div/img')
    except:
        print("Error Transfer_money")
        print("ctl00_lbtnLogoHomePage")
        time.sleep(3)
        pass


if __name__ == "__main__":
    today_date = datetime.date.today()
    new_today_date = today_date.strftime("%d/%m/%Y")
    dc = Drive_Control.Drive_Control()
    is_ok = True
    username = "panunpob"
    password = "Bb112233@"
    t0 = 0
    t1 = 0
    login(
        "https://www.krungsribizonline.com/BAY.KOL.Corp.WebSite/Common/Login.aspx",
        username,
        password,
    )

    options = {
        0: zero,
        1: one,
        2: two,
        3: three,
        # 4: four,
    }
    round_number = 0
    while True:
        round_number+=1
        try:
            print("round :",round_number, datetime.datetime.today())
            if is_ok:
                t0 = time.time()
            else:
                is_ok = True if ((1.5* 60) < (t1 - t0)) else False
                if is_ok:
                    t0 = time.time()
                t1 = time.time()
            if is_ok == False:
                randomshit(options)
            elif is_ok == True:
                is_ok = False
                while 1:
                    click_menu = dc.find_xpath(
                        '//*[@id="ctl00_pnlMainMenu"]/div/div[@class="menu_main menu_expanded"]'
                    )
                    if len(click_menu) > 0:
                        time.sleep(3)
                        in_menu = dc.find_xpath(
                            "/html/body/div/form/div[6]/div/div[1]/div[4]/div/div[4]/a[1]"
                        )
                        if len(in_menu) > 0:
                            in_menu[0].click()
                            break
                    else:
                        click_menu = dc.find_xpath(
                            '//*[@id="ctl00_pnlMainMenu"]/div/div[3]'
                        )
                        click_menu[0].click()
                dc.execut_script("window.scrollTo(0, document.body.scrollHeight);")
                dep = []
                withdraw = []
                dataAccMe = []
                dataBankType = []
                dataDate = []
                dataTime = []
                dataMoney = []
                dataAcc = []
                pagination_onpage = 1
                do_loop = True
                while do_loop == True:
                    time.sleep(random.randint(1, 3))
                    # ตารางข้อมูลที่มีเวลา
                    statements = dc.find_xpath('//*[@id="ctl00_cphSectionData_gvTodayTransaction"]/tbody/tr')[1:]
                    for row in statements:
                        x = (row.text).split(" ")
                        print(x)
                        if x[2] == "TN":
                            dataAccMe.append(Acc)
                            dataBankType.append(BankType)
                            dataDate.append(x[0])
                            dataTime.append(x[1])
                            dataMoney.append(x[4])
                            dataAcc.append(x[3])
                    if pagination_onpage < 2:
                        have_page = dc.find_xpath('//*[@id="ctl00_cphSectionData_updContent"]/div[11]/a[5]')
                        if len(have_page) > 0:
                            dc.click_xpath('//*[@id="ctl00_cphSectionData_updContent"]/div[11]/a[5]')
                        else:
                            do_loop = False
                        pagination_onpage += 1
                    else:
                        next_page = None
                        next_page = dc.find_xpath('//*[@id="ctl00_cphSectionData_updContent"]/div[11]/a[7]')
                        if len(next_page) > 0:
                            dc.click_xpath('//*[@id="ctl00_cphSectionData_updContent"]/div[11]/a[7]')
                        else:
                            do_loop = False
                        pagination_onpage += 1
                save_statement(dataAccMe, dataBankType, dataDate, dataTime, dataMoney, dataAcc)
                t1 = time.time()
            time.sleep(random.randint(3, 10))
        except :
            print('***  **************  ***')
            print('*****  **********  *****')
            print('*******  ******  *******')
            print('*********  **  *********')
            print('***********  ***********')
            print('*********  **  *********')
            print('*******  ******  *******')
            print('*****  **********  *****')
            print('***  **************  ***')
            time.sleep(random.randint(2,5))
            dc.driver.close()
            # time.sleep(random.randint(60,1800))
            time.sleep(random.randint(30,180))
            dc = Drive_Control.Drive_Control()
            is_ok = True
            t0 = 0
            t1 = 0
            login(
                "https://www.krungsribizonline.com/BAY.KOL.Corp.WebSite/Common/Login.aspx",
                username,
                password,
            )