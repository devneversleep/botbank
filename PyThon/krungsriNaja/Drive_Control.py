from multiprocessing import Lock, Process, Queue, current_process
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

import numpy as np
import queue  # imported for using queue.Empty exception
import time


class Drive_Control:
    def __init__(self):
        self.driver = webdriver.Chrome(ChromeDriverManager().install())
        self.driver.maximize_window()
        self.windows_size = self.driver.get_window_size()
        self.exit = 0

    def Setup(self):
        pass

    def escbutton(self):
        return self.driver.find_element_by_xpath("//body").send_keys(Keys.ESCAPE)

    def refreshPage(self):
        return self.driver.find_element_by_xpath("//body").send_keys(Keys.CONTROL + "r")

    def keypressUP(self):
        return self.driver.find_element_by_xpath("//body").send_keys(Keys.ARROW_UP)

    def keypressDOWN(self):
        return self.driver.find_element_by_xpath("//body").send_keys(Keys.ARROW_DOWN)

    def get_by_element(self, element, key):
        return self.driver.find_element(element, key)

    def open_web(self, web_site):
        self.driver.get(web_site)

    def find_css(self, element):
        return self.driver.find_element_by_css_selector(element)

    def find_id(self, element):
        return self.driver.find_element_by_id(element)

    def find_name(self, element):
        return self.driver.find_element_by_name(element)

    def find_xpath(self, element):
        return self.driver.find_elements_by_xpath(element)

    def switch_tab(self, tab):
        return self.driver.switch_to_window(self.driver.window_handles[tab])

    def refresh(self):
        return self.driver.refresh()

    def send_key_css(self, element, key):
        return self.driver.find_element_by_css_selector(element).send_keys(key)

    def send_key_id(self, element, key):
        return self.driver.find_element_by_id(element).send_keys(key)

    def send_key_name(self, element, key):
        return self.driver.find_element_by_name(element).send_keys(key)

    def execut_script(self, element):
        return self.driver.execute_script(element)

    def click_xpath(self, element):
        return self.driver.find_elements_by_xpath(element)[0].click()