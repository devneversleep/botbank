import requests
import pandas as pd
import math
import time
import datetime
import datetime
from flask_apscheduler import APScheduler
from apscheduler.triggers.cron import CronTrigger


class transaction:
    def __init__(self, Id, Acc_me, BankType, Date, Time, Money, Acc, Confirm_status):
        self.Id = Id
        self.Acc_me = Acc_me
        self.BankType = BankType
        self.Date = Date
        self.Time = Time
        self.Money = Money
        self.Acc = Acc
        self.Confirm_status = Confirm_status

    def __repr__(self):
        return (
            "Acc_me: %s \nBankType: %s \nDate: %s \nTime: %s \nMoney: %s \nAcc: %s \nconfirm: %s"
            % (
                self.Acc_me,
                self.BankType,
                self.Date,
                self.Time,
                self.Money,ด
                self.Acc,
                self.Confirm_status
            )
        )


def allActions(action, dep_trans_id=None):
    URL = "https://eieitech.co/system/required/api/main/call.php"

    if action == "pending_deposit_list":
        PARAMS = {
            "code": "xlqji",
            "api": "f0ff61a78738b36a4670e09806bfeca2",
            "action": "bot_get_pending_list",
            "type_name": "pending_deposit_list",
            "status": "pending",
        }
        r = requests.get(url=URL, params=PARAMS)
        data = r.json()

        # print(data)
        dep_trans_list = data["response_data"]["deposit_trans_list"]["response_data"][
            "trans_list"
        ]

        deposits = []
        if len(dep_trans_list) > 0:
            for dep in dep_trans_list:
                tran = transaction(
                    dep["id"],
                    "".join(dep["system_account_no"].split("-")),
                    dep["system_bank_name_en"],
                    dep["insert_date_time"].split(" ")[0],
                    dep["insert_date_time"].split(" ")[1],
                    dep["price"],
                    "".join(dep["customer_bank_acc_no"].split("-")),
                    dep["customer_confirm_status"]
                )
                deposits.append(tran)
                # print(dep["trans_no"])
            return deposits
        else:
            print("No pending deposit")
        # print(deposits)

    if action == "approve_pending_deposit" and dep_trans_id != None:
        # print("done")
        PARAMS = {
            "code": "xlqji",
            "api": "f0ff61a78738b36a4670e09806bfeca2",
            "action": "bot_get_pending_list",
            "type_name": "approve_pending_deposit",
            "dep_trans_id": dep_trans_id,
        }
        r = requests.post(url=URL, params=PARAMS)
        data = r.json()
        is_ok = data["response_data"]["is_success"]["response_status"]
        if is_ok:
            return "approve dep: " + dep_trans_id
        else:
            return "something wrong"

    if action == "return_pending_deposit" and dep_trans_id != None:
        PARAMS = {
            "code": "xlqji",
            "api": "f0ff61a78738b36a4670e09806bfeca2",
            "action": "bot_return_deposit",
            "bot_id": 1,
            "trans_id": dep_trans_id,
            "reason": "neversleep return dep",
            "wait_confirm": 0,
        }

        r = requests.post(url=URL, params=PARAMS)
        data = r.json()
        print("returned to admin by NVSL")

    if action == "deposit_list_wait_confirm":
        PARAMS = {
            "code": "xlqji",
            "api": "f0ff61a78738b36a4670e09806bfeca2",
            "action": "bot_get_pending_list",
            "type_name": "deposit_list_wait_confirm",
            "status": "wait_confirm",
        }

        r = requests.get(url=URL, params=PARAMS)
        data = r.json()

        dep_trans_list = data["response_data"]["deposit_trans_list"]["response_data"][
            "trans_list"
        ]

        deposits = []
        if len(dep_trans_list) > 0:
            for dep in dep_trans_list:
                tran = transaction(
                    dep["id"],
                    "".join(dep["system_account_no"].split("-")),
                    dep["system_bank_name_en"],
                    dep["insert_date_time"].split(" ")[0],
                    dep["insert_date_time"].split(" ")[1],
                    dep["price"],
                    "".join(dep["customer_bank_acc_no"].split("-")),
                    dep["customer_confirm_status"]
                )
                deposits.append(tran)
            # print(len(deposits))
            return deposits
        else:
            print("No wait_confirm deposit")

    if action == "approve_wait_confirm_deposit" and dep_trans_id != None:
        PARAMS = {
            "code": "xlqji",
            "api": "f0ff61a78738b36a4670e09806bfeca2",
            "action": "bot_get_pending_list",
            "type_name": "approve_wait_confirm_deposit",
            "dep_trans_id": dep_trans_id,
        }
        r = requests.post(url=URL, params=PARAMS)
        data = r.json()
        is_ok = data["response_data"]["is_success"]["response_status"]
        if is_ok:
            return "approve wait dep: " + dep_trans_id
        else:
            return "something wrong"

    if action == "return_wait_confirm_deposit" and dep_trans_id != None:
        PARAMS = {
            "code": "xlqji",
            "api": "f0ff61a78738b36a4670e09806bfeca2",
            "action": "bot_return_deposit",
            "bot_id": 1,
            "trans_id": dep_trans_id,
            "reason": "neversleep return wait_confirm dep",
            "wait_confirm": 1,
        }

        r = requests.post(url=URL, params=PARAMS)
        data = r.json()
        print("returned to admin by NVSL")


def deposit_statement():
    deposit_list = []
    s = datetime.datetime.today().strftime("%Y-%m-%d")
    df = pd.read_excel("krungsri_statement_" + s + ".xlsx")
    # df = pd.read_excel("../krungsriNaja/krungsri_statement.xlsx")
    if len(df) > 0:
        for index, row in df.iterrows():
            Acc_me = row["Acc"]
            BankType = row["BankType"]
            year = str(int(row["Date"].split("/")[2]) - 543)
            Date = row["Date"].split("/")[0] + "/" + row["Date"].split("/")[1] + "/" + year
            Time = row["Time"]
            Money = row["Money"]
            Acc = row["Acc"]
            dep = transaction(None, Acc_me, BankType, Date, Time, Money, Acc, "statement")
            deposit_list.append(dep)
        return deposit_list
    else:
        print("No transaction in excel file")

def in_between(now, start, end):
    if start <= end:
        return start <= now < end
    else:
        return start <= now or now < end

def checkMatchedStatement(statement, dep_list_api):
    from_database = dep_list_api
    from_statement = statement

    for dep_db_index, dep_db in enumerate(from_database):
        last_five_acc_db = str(dep_db.Acc)[-5:]
        price_db = int(float(dep_db.Money))
        date_db = dep_db.Date
        time_db = dep_db.Time
        confirm_status_db = dep_db.Confirm_status
        date_time_db = time.mktime(
            datetime.datetime.strptime(
                date_db + " " + time_db, "%Y-%m-%d %H:%M:%S"
            ).timetuple()
        )
        time_ok = True if time.time() - date_time_db < 900 else False
        if time_ok == False:
            print ('time not ok')
            if confirm_status_db == "1":
                allActions("return_pending_deposit", dep_db.Id)
                break
            if confirm_status_db == "0":
                allActions("return_wait_confirm_deposit", dep_db.Id)
                break
        for dep_st_index, dep_st in enumerate(from_statement):
            last_five_acc_st = str(dep_st.Acc)[-5:]
            price_st = math.floor(dep_st.Money)
            date_st = dep_st.Date
            time_st = dep_st.Time
            date_time_st = time.mktime(
                datetime.datetime.strptime(
                    date_st + " " + time_st, "%d/%m/%Y %H:%M"
                ).timetuple()
            )
            time_in_range = in_between(int(date_time_db), int(date_time_st) - 900, int(date_time_st) + 900)
            # print ("--------------------------")
            # print (last_five_acc_db, last_five_acc_st)
            # print (price_db, price_st)
            # print (time_in_range)
            # print (confirm_status_db)
            if last_five_acc_db == last_five_acc_st and price_db == price_st and time_in_range:
                if confirm_status_db == "1":
                    print("Approve transaction: " + dep_db.Id)
                    allActions("approve_pending_deposit", dep_db.Id)
                if confirm_status_db == "0":
                    print("Approve transaction: " + dep_db.Id)
                    allActions("approve_wait_confirm_deposit", dep_db.Id)
                break
            # else:
            #     print("transaction not matched")


def scheduleTask():
    dep_list_stm = deposit_statement()
    dep_wait_confirm_list = [] if allActions("deposit_list_wait_confirm", None) == None else allActions("deposit_list_wait_confirm", None) 

    dep_pending_list = [] if allActions("pending_deposit_list", None) == None else allActions("pending_deposit_list", None)
    dep_all = dep_wait_confirm_list + dep_pending_list
    checkMatchedStatement(dep_list_stm, dep_all)

try:
    scheduler = APScheduler()
    scheduler.add_job(id="Tasks", func=scheduleTask, trigger="interval", seconds=10)
    scheduler.start()
except:
    print('***  **************  ***')
    print('*****  **********  *****')
    print('*******  ******  *******')
    print('*********  **  *********')
    print('***********  ***********')
    print('*********  **  *********')
    print('*******  ******  *******')
    print('*****  **********  *****')
    print('***  **************  ***')

if __name__ == "__main__":
    while 1:
        pass
