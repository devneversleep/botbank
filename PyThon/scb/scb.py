import Drive_Control
import time
import datetime
import random
from selenium.webdriver.common.by import By
import xlsxwriter
import pandas as pd
from datetime import date


def send_keys_element(element, command):
    for x in command:
        element.send_keys(str(x))
        time.sleep(random.randint(0, 10) / 10)


def GetStatement(in_case):
    print("on GetStatement Fx")
    tboby = dc.find_xpath('//*[@id="DataProcess_GridView"]/tbody/tr')[1:]
    data_income = []
    dataDate = []
    dataTime = []
    dataMoney = []
    dataAcc = []

    for target_list in tboby:
        data_ary = []
        if in_case == "today":
            data_ary.append(target_list.find_element(By.XPATH, "./td[1]").text)
            data_ary.append(target_list.find_element(By.XPATH, "./td[2]").text)
            data_ary.append(target_list.find_element(By.XPATH, "./td[6]").text)
            data_ary.append(target_list.find_element(By.XPATH, "./td[7]").text)

            if data_ary[2] != " ":
                dataDate.append(target_list.find_element(By.XPATH, "./td[1]").text)
                dataTime.append(target_list.find_element(By.XPATH, "./td[2]").text)
                dataMoney.append(target_list.find_element(By.XPATH, "./td[6]").text)
                dataAcc.append(target_list.find_element(By.XPATH, "./td[7]").text)
        else:
            data_ary.append(target_list.find_element(By.XPATH, "./td[1]").text)
            data_ary.append(target_list.find_element(By.XPATH, "./td[2]").text)
            data_ary.append(target_list.find_element(By.XPATH, "./td[8]").text)
            data_ary.append(target_list.find_element(By.XPATH, "./td[5]").text)

            if data_ary[2] != " ":
                dataDate.append(target_list.find_element(By.XPATH, "./td[1]").text)
                dataTime.append(target_list.find_element(By.XPATH, "./td[2]").text)
                dataMoney.append(target_list.find_element(By.XPATH, "./td[8]").text)
                dataAcc.append(target_list.find_element(By.XPATH, "./td[5]").text)

        # เอาแต่เงินเข้า
        if data_ary[2] != " ":
            data_income.append(data_ary)

    # เอาค่ารวมท้ายตารางออก //*[@id="mainpage"]
    if (
        dc.find_xpath(
            "/html/body/table[3]/tbody/tr/td/table[1]/tbody/tr[4]/td[3]/table/tbody/tr[1]/td/div/b[2]"
        )
        == "Sorry, you cannot do this transaction at the moment. Please try again later."
    ):
        dc.click_xpath('//*[@id="mainpage"]')
        return "no_statement"
    else:
        if not data_income:
            dc.click_xpath('//*[@id="back"]')
            return "no_statement"
        else:
            data_income.pop()
            dataDate.pop()
            dataTime.pop()
            dataMoney.pop()
            dataAcc.pop()
            return data_income, dataDate, dataTime, dataMoney, dataAcc


def present_statement():
    print("on present_statement Fx")
    try:
        dc.click_xpath('//*[@id="DataProcess_Link2"]')
        # หน่วเวลาที่หน้า statement
        time.sleep(random.randint(7, 45))
        return GetStatement("today")
    except:
        Pre_Sta = "no_statement"
        print("Bug present_statement can not get in statement Fx")
        return Pre_Sta


def HistoricalStatement(numberForX):
    print("on HistoricalStatement Fx")

    dc.click_xpath('//*[@id="DataProcess_Link3"]')
    time.sleep(random.randint(7, 45))
    dc.click_xpath('//*[@id="DataProcess_ddlMonth"]/option[' + str(numberForX) + "]")
    time.sleep(random.randint(7, 45))
    return GetStatement("historical")


def save_statement(dataAccMe, dataBankType, dataDate, dataTime, dataMoney, dataAcc):
    print("on save_statement Fx")

    # today = date.today()
    # สร้าง DataFrame ที่มี 1 คอลัมน์ชื่อ 'Data'
    dataframe3 = pd.DataFrame(
        {
            "Acc_Me": dataAccMe,
            "BankType": dataBankType,
            "Date": dataDate,
            "Time": dataTime,
            "Money": dataMoney,
            "Acc": dataAcc,
        }
    )
    # สร้าง Pandas Excel Writer เพื่อใช้เขียนไฟล์ Excel โดยใช้ Engine เป็น xlsxwriter
    # โดยตั้งชื่อไฟล์ว่า 'simple_data.xlsx'
    writer = pd.ExcelWriter(
        "scb_statement_" + str(date.today()) + ".xlsx", engine="xlsxwriter"
    )
    # นำข้อมูลที่สร้างไว้ในตัวแปร dataframe เขียนลงไฟล์
    dataframe3.to_excel(writer, sheet_name="kbank")
    # จบการทำงาน Pandas Excel writer และเซฟข้อมูลออกมาเป็นไฟล์ Excel
    writer.save()
    print("Save Done.")


def DoSomeThingElse(number_of_task):
    print("on DoSomeThingElse Fx")
    if number_of_task == 0:
        print("on DoSomeThingElse 0 Fx")
        dc.click_xpath('//*[@id="FundTransfer_Image"]')
        time.sleep(random.randint(7, 45))
        dc.click_xpath('//*[@id="ctl15_History_LinkButton"]')
        time.sleep(random.randint(5, 15))
        dc.click_xpath('//*[@id="DataProcess_ctl00_OWN_LinkButton"]')
        time.sleep(random.randint(5, 15))
        dc.click_xpath(
            "/html/body/form/table[4]/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/table[1]/tbody/tr[1]/td[3]/a/img"
        )
        time.sleep(random.randint(7, 45))
        # return True
    elif number_of_task == 1:
        print("on DoSomeThingElse 1 Fx")
        dc.click_xpath('//*[@id="FundTransfer_Image"]')
        time.sleep(random.randint(7, 45))
        dc.click_xpath('//*[@id="ctl15_History_LinkButton"]')
        time.sleep(random.randint(5, 15))
        dc.click_xpath('//*[@id="DataProcess_ctl00_TRD_LinkButton"]')
        time.sleep(random.randint(5, 15))
        dc.click_xpath('//*[@id="go"]')
        time.sleep(random.randint(7, 45))
        # return True
    elif number_of_task == 2:
        print("on DoSomeThingElse 2 Fx")
        dc.click_xpath('//*[@id="FundTransfer_Image"]')
        time.sleep(random.randint(7, 45))
        dc.click_xpath('//*[@id="ctl15_History_LinkButton"]')
        time.sleep(random.randint(5, 15))
        dc.click_xpath('//*[@id="DataProcess_ctl00_FTA_LinkButton"]')
        time.sleep(random.randint(5, 15))
        dc.click_xpath('//*[@id="go"]')
        # dc.execut_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(random.randint(7, 45))
        # return True

    dc.click_xpath('//*[@id="MyPage_Image"]')
    print("exit Do somgthing")


def Transfer_money():
    dc.click_xpath('//*[@id="FundTransfer_Image"]')
    time.sleep(random.randint(5, 10))
    dc.click_xpath('//*[@id="ctl15_AnotherBankAccount_LinkButton"]')
    time.sleep(random.randint(5, 10))
    dc.click_xpath(
        "/html/body/form/table[4]/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/select"
    )
    time.sleep(random.randint(1, 4))
    dc.click_xpath(
        "/html/body/form/table[4]/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/select/option"
    )
    time.sleep(random.randint(3, 6))
    money_input = dc.find_name("ctl00$DataProcess$CustAmount_TextBox")
    send_keys_element(money_input, str(random.randint(2, 3) / 1.7))
    time.sleep(random.randint(5, 10))
    dc.click_xpath('//*[@id="nxt"]')
    time.sleep(random.randint(5, 15))
    dc.click_xpath('//*[@id="cnfrm"]')
    time.sleep(random.randint(5, 10))
    dc.click_xpath('//*[@id="MyPage_Image"]')
    # dc.click_xpath('//*[@id="ok"]')
    # OTP
    # ctl00$DataProcess$txtCustOTP
    # //*[@id="actvt1"]


def Login():
    print("on Login Fx")
    username = "panunrat"
    password = "Aa112233"
    # dc = Drive_Control.Drive_Control()
    dc.open_web("https://www.scbeasy.com/v1.4/site/presignon/index.asp")
    time.sleep(random.randint(2, 6))
    dc.send_key_id("LOGIN", username)
    time.sleep(random.randint(2, 6))
    dc.send_key_name("PASSWD", password)
    time.sleep(random.randint(1, 3))
    dc.click_xpath("//input[@id='lgin']")
    time.sleep(random.randint(5, 8))
    dc.click_xpath('//*[@id="Image3"]')
    time.sleep(random.randint(5, 8))
    dc.click_xpath('//*[@id="MyPage_Image"]')


if __name__ == "__main__":
    dc = Drive_Control.Drive_Control()
    Login()
    x86 = 0
    while 1:
        print(datetime.datetime.today())
        try:
            Transfer_money()
            # time.sleep(random.randint(4,8))
            dc.click_xpath('//*[@id="ctl12_lnk1"]')
            time.sleep(random.randint(4, 8))
            # dc.click_xpath('//*[@id="DataProcess_SaCaGridView_SaCaView_LinkButton_0"]')
            dc.click_xpath('//*[@id="DataProcess_SaCaGridView_SaCa_LinkButton_0"]')
            time.sleep(random.randint(3, 6))
            Pre_Sta = present_statement()
            time.sleep(random.randint(3, 6))
            if Pre_Sta != "no_statement":
                AC_Name = []
                AC_Type = []
                for x in Pre_Sta[1]:
                    AC_Name.append("8422425305")
                    AC_Type.append("SCB")
                save_statement(
                    AC_Name, AC_Type, Pre_Sta[1], Pre_Sta[2], Pre_Sta[3], Pre_Sta[4]
                )
                time.sleep(3.5)
                dc.click_xpath('//*[@id="DataProcess_Link1"]')
                time.sleep(random.randint(3, 15))
                dc.click_xpath('//*[@id="ctl15_lnk1"]')
            print(x86)
            x86 += 1
            DoSomeThingElse(random.randint(0, 3))
            time.sleep(random.randint(3, 6))
        except:
            print("***  **************  ***")
            print("*****  **********  *****")
            print("*******  ******  *******")
            print("*********  **  *********")
            print("***********  ***********")
            print("*********  **  *********")
            print("*******  ******  *******")
            print("*****  **********  *****")
            print("***  **************  ***")
            time.sleep(random.randint(2, 5))
            dc.driver.close()
            # time.sleep(random.randint(60,1800))
            time.sleep(random.randint(30, 180))
            dc = Drive_Control.Drive_Control()
            Login()
        # time.sleep(random.randint(30,180))

    # for x in range(2,13):
    #     try:
    #         Historical_Statement = HistoricalStatement(x)
    #         time.sleep(2)
    #         for x in Historical_Statement:
    #             print(x)
    #     except:
    #         time.sleep(4)
    #         dc.escbutton()
    #         pass