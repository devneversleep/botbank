from multiprocessing import Lock, Process, Queue, current_process
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.utils import ChromeType
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.microsoft import EdgeChromiumDriverManager

# import numpy as np
import queue # imported for using queue.Empty exception
import time

class Drive_Control():
    def __init__(self):
        self.driver = webdriver.Chrome(ChromeDriverManager().install())
        # self.driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())
        # self.driver = webdriver.Edge(EdgeChromiumDriverManager().install())
        self.driver.maximize_window()
        self.windows_size = self.driver.get_window_size()
        self.actionchains = ActionChains(self.driver) # initialize ActionChain object
        self.wait = WebDriverWait(self.driver, 10)
        self.ec = ec
        self.Keys = Keys
        self.By = By
        self.exit = 0
        self.ActionChains = ActionChains


    def Setup(self):
        pass

    def escbutton(self):
        return self.driver.find_element_by_xpath('//body').send_keys(Keys.ESCAPE)

    def get_by_element(self,element,key):
        return self.driver.find_element(element,key)
        
    def open_web(self,web_site):
        self.driver.get(web_site)

    def find_element(self,element,command):
        return self.driver.find_element(element,command)

    def find_css(self,element):
        return self.driver.find_element_by_css_selector(element)

    def find_id(self,element):
        return self.driver.find_element_by_id(element)

    def find_name(self,element):
        return self.driver.find_element_by_name(element)

    def find_xpath(self,element):
        return self.driver.find_elements_by_xpath(element)

    def switch_tab(self,tab):
        return self.driver.switch_to_window(self.driver.window_handles[tab])

    def refresh(self):
        return self.driver.refresh()

    def send_key_css(self,element,key):
        return self.driver.find_element_by_css_selector(element).send_keys(key)

    def send_key_id(self,element,key):
        return self.driver.find_element_by_id(element).send_keys(key)

    def send_key_name(self,element,key):
        return self.driver.find_element_by_name(element).send_keys(key)

    def execut_script(self,element):
        return self.driver.execute_script(element)

    def click_xpath(self,element):
        return self.driver.find_elements_by_xpath(element)[0].click()