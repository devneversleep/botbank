# from crontab import CronTab

# cron = CronTab()
# job = cron.new(command="python ./krungsriNaja/deposit_api.py")
# job.minute.every(1)

# cron.write()


from flask_apscheduler import APScheduler
from apscheduler.triggers.cron import CronTrigger


def scheduleTask():
    print("This test runs every 1 seconds")


scheduler = APScheduler()
scheduler.add_job(id="Tasks", func=scheduleTask, trigger="interval", seconds=59)
scheduler.start()

if __name__ == "__main__":
    while 1:
        pass